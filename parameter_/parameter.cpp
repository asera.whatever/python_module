#include <Python.h>
#include <iostream>

/*
 * printParameter print 2 integer parameter
 */
static PyObject *printParameter(PyObject *self, PyObject *args)
{
    int x;
    int y;

    if (!PyArg_ParseTuple(args, "ii", &x, &y))
        return NULL;
    std::cout << "first parameter: " << x << std::endl;
    std::cout << "second parameter: " << y << std::endl;
    return Py_None;
}

static int storedParameter = 0;

static PyObject *storeParameter(PyObject *self, PyObject *args)
{
    if (!PyArg_ParseTuple(args, "i", &storedParameter))
        return NULL;
    return Py_None;
}

static PyObject *printStoredParameter(PyObject *self, PyObject *args)
{
    std::cout << storedParameter << std::endl;
    return Py_None;
}

/*
 * parameterMethod define the module's function
 */
static PyMethodDef parameterMethod[] = {
        {"printParameter",  printParameter, METH_VARARGS,  "print 2 integer parameter!."},
        {"storeParameter",  storeParameter, METH_VARARGS,  "store 1 integer parameter!."},
        {"printStoredParameter",  printStoredParameter, METH_VARARGS,  "print the stored parameter!."},
        {NULL, NULL, 0, NULL}
};

/*
 * parameterModule define the module
 */
static struct PyModuleDef parameterModule = {
        PyModuleDef_HEAD_INIT,  /* wtf */
        "parameter",                /* name of module */
        NULL,                   /* module documentation, may be NULL */
        -1,                     /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. idk what that means*/
        parameterMethod
};

/*
 * PYInit_parameter initialize the ParameterWorld module
 */
PyMODINIT_FUNC PyInit_parameter(void)
{
    return PyModule_Create(&parameterModule);
}