#include <Python.h>
#include <iostream>

/*
 * getHelloWorld return a "Hello World!" python string object
 */
static PyObject *getHelloWorld(PyObject *self, PyObject *args)
{
    return Py_BuildValue("s", "Hello World but in cpp!");
}

/*
 * helloWorld print "Hello World!"
 */
static PyObject *helloWorld(PyObject *self, PyObject *args)
{
    std::cout << "Hello World but with cout!" << std::endl;
    return Py_None;
}

/*
 * helloMethod define the module's function
 */
static PyMethodDef helloMethod[] = {
        {"hello",  helloWorld, METH_VARARGS,  "Print Hello World!."},
        {"getHello",  getHelloWorld, METH_VARARGS,  "Return Hello World!."},
        {NULL, NULL, 0, NULL}
};

/*
 * helloModule define the module
 */
static struct PyModuleDef helloModule = {
        PyModuleDef_HEAD_INIT,  /* wtf */
        "hello",                /* name of module */
        NULL,                   /* module documentation, may be NULL */
        -1,                     /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. idk what that means*/
        helloMethod
};

/*
 * PYInit_hello initialize the HelloWorld module
 */
PyMODINIT_FUNC PyInit_hello(void)
{
    return PyModule_Create(&helloModule);
}