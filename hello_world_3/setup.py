from distutils.core import setup, Extension

module1 = Extension('hello',
                    sources = ['hello_world.cpp'])

setup (name = 'hello',
       version = '1.0',
       description = 'This is a hello package',
       ext_modules = [module1])