#include <Python.h>
#include <iostream>

#include <SFML/Graphics.hpp>

/*
 * Framebuffer class definition
 */
class Framebuffer
{
 public:
 	Framebuffer(unsigned int width, unsigned int height):
 	_width(width),
 	_height(height),
 	_pixels()
 	{
 		_pixels = new sf::Uint8[width * height * 4];
 	};
 	~Framebuffer(){};
 	void putPixel(unsigned int x, unsigned int y, sf::Color color) {
		if (x < _width && y < _width) {
			_pixels[(_width * y + x) * 4 + 0] = color.r; 
			_pixels[(_width * y + x) * 4 + 1] = color.g; 
			_pixels[(_width * y + x) * 4 + 2] = color.b; 
			_pixels[(_width * y + x) * 4 + 3] = color.a; 
		} 		
 	}

 	sf::Uint8 *getPixels() const {
 		return _pixels;
 	}
 private:
	unsigned int _width;
	unsigned int _height;
	sf::Uint8 *_pixels;
};

/*
 * End of Framebuffer class definition
 */

static unsigned int width = 0;
static unsigned int height = 0;
static Framebuffer *framebuffer = NULL;
static sf::RenderWindow *window = NULL;
static sf::Texture *texture = NULL;

static PyObject *init(PyObject *self, PyObject *args)
{
	if (!PyArg_ParseTuple(args, "ii", &width, &height))
		return NULL;

	framebuffer = new Framebuffer(width, height);
	texture = new sf::Texture();
	texture->create(width, height);
	window = new sf::RenderWindow(sf::VideoMode(width, height), "MiniSfml", sf::Style::Resize | sf::Style::Close);
        Py_RETURN_NONE;
}

static PyObject *putPixel(PyObject *self, PyObject *args) {
	int x;
	int y;

	int r;
	int g;
	int b;
	if (framebuffer == NULL)
		return NULL;
	if (!PyArg_ParseTuple(args, "iiiii", &x, &y, &r, &g, &b))
		return NULL;
	framebuffer->putPixel(x, y, sf::Color(r, g, b));
        Py_RETURN_NONE;
}

/*
 * 
 */
static PyObject *drop(PyObject *self, PyObject *args) {
	if (framebuffer == NULL || window == NULL)
		return NULL;
	delete framebuffer;
	delete window;
        Py_RETURN_NONE;
}

/*
 * 
 */
static PyObject *isActive(PyObject *self, PyObject *args) {
	if (window == NULL)
		return NULL;
	if (window->isOpen())
		Py_RETURN_TRUE;
	Py_RETURN_FALSE;
}


/*
 * 
 */
static PyObject *update(PyObject *self, PyObject *args) {
	if (window == NULL || framebuffer == NULL || texture == NULL)
		return NULL;
	

	sf::Event event;
        while (window->pollEvent(event))
            if (event.type == sf::Event::Closed)
                window->close();
	

        sf::Sprite sprite;

        texture->update(framebuffer->getPixels(), width, height, 0, 0);
        sprite.setTexture(*texture, true);
        window->draw(sprite);
	window->display();

        Py_RETURN_NONE;
}

/*
 * miniSfmlMethod define the module's function
 */
static PyMethodDef miniSfmlMethod[] = {
		{"init", 	init, 		METH_VARARGS, "init the display"}, /* work */
		{"putPixel", 	putPixel, 	METH_VARARGS, "color a pixel at the x / y coord with the rgba color given"}, /* work */
		{"drop", 	drop, 		METH_VARARGS, "drop the display"}, /* work */
		{"isActive", 	isActive, 	METH_VARARGS, "return true if the display is still active"}, /* work */
		{"update", 	update, 	METH_VARARGS, "update the display and accept input"}, /* work */
		{NULL, NULL, 0, NULL}
};

/*
 * miniSfmlModule define the module
 */
static struct PyModuleDef miniSfmlModule = {
		PyModuleDef_HEAD_INIT,  /* wtf */
		"miniSfml",                /* name of module */
		NULL,                   /* module documentation, may be NULL */
		-1,                     /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. idk what that means*/
		miniSfmlMethod
};

/*
 * PYInit_miniSfml initialize the miniSfmlWorld module
 */
PyMODINIT_FUNC PyInit_miniSfml(void)
{
	return PyModule_Create(&miniSfmlModule);
}
