from distutils.core import setup, Extension

module1 = Extension('miniSfml',
                    sources=['mini_sfml.cpp'],
                    libraries = ['sfml-window', 'sfml-graphics', 'sfml-system'],
                    library_dirs = ['/usr/lib'])

setup(name='miniSfml',
       version='1.0',
       description='test',
       ext_modules=[module1])
